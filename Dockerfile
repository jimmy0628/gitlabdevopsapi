FROM golang:1.12.7
RUN go get -u github.com/gin-gonic/gin
RUN git clone https://gitlab.com/jimmy0628/gitlabdevopsapi.git
CMD ["go run ./gitlabdevopsapi/start_service.go"] 

