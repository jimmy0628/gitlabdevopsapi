# GitlabDevOpsAPI
This is a web service that wraps the gitlab API to setup the DevOps best practices quickly.

This service is written in Go, and all related tools and functions are from public cloud.

Cloud Service below are targeted in the MVP

1. AWS
2. Digital Ocean
